<?php
	get_header(); 
?>
<div class="page-body body"
	<?php if ( has_post_thumbnail() ): ?>
		style="background-image: url(<?php the_post_thumbnail_url('full');?>)"
	<?php endif; ?>
>
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
	        <div class="main-select-wrap">
	        	<h1 class="page-title text-center">
        			<span><?php the_title(); ?></span>
        		</h1>
        		<br />

        		<div class="page-content">
            		<?php the_content(); ?>
            		
            	</div>

	           
	        </div>

	    </div>
	    
		    
	<?php endwhile ; ?>
	<?php get_footer(); ?>
</div>

