<div id="last-selection" class="animate last-selection with-insta" data-section-name="last-selection">
    <div  class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="last-box">
                    <h1>Blog</h1>
                    <?php
                        $recentPostArgs = array(
                            'numberposts' => 5,
                            'offset' => 0,
                            'category' => 0,
                            'orderby' => 'post_date',
                            'order' => 'DESC',
                            'include' => '',
                            'exclude' => '',
                            'meta_key' => '',
                            'meta_value' =>'',
                            'post_type' => 'post',
                            'post_status' => 'publish',
                            'suppress_filters' => true
                        );

                        $recentPosts = wp_get_recent_posts( $recentPostArgs );
                        ?>
                    <div class="row blog">
                        <?php if(isset($recentPosts[0])): ?>
                            <div class="col-sm-4">
                                <div class="blog-box imageA">
                                    <?php  if($featuredImg1 = get_the_post_thumbnail_url($recentPosts[0]['ID'], 'full') ) : ?>
                                        <div class="blog-image blog-image-1">
                                            <img src="<?php echo $featuredImg1; ?>" class="img-responsive" />
                                        </div>
                                    <?php endif; ?>
                                    <div class="blog-text">
                                        <h3><a href="<?php echo get_permalink($recentPosts[0]['ID']); ?>"><?php echo $recentPosts[0]['post_title'];?></a></h3>
                                        <div class="author"><a href="<?php echo get_permalink($recentPosts[0]['ID']); ?>"><?php echo get_the_author_meta('display_name', $recentPosts[0]['post_author']) ;?>  |  <?php echo date('M d, Y', strtotime($recentPosts[0]['post_date']) ) ?></a></div>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>

                        <?php if(count($recentPosts) > 1 ): ?>
                            <div class="col-sm-8 second-box">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <?php if(isset($recentPosts[1])):?>
                                        <div class="blog-box change">
                                            <div class="blog-text white">
                                                <h3><a href="<?php echo get_permalink($recentPosts[1]['ID']); ?>"><?php echo $recentPosts[1]['post_title'];?></a></h3>
                                                <div class="author"><a href="<?php echo get_permalink($recentPosts[1]['ID']); ?>"><?php echo get_the_author_meta('display_name', $recentPosts[1]['post_author']) ;?>  |  <?php echo date('M d, Y', strtotime($recentPosts[1]['post_date']) ) ?></a></div>
                                            </div>
                                        </div>
                                        <?php endif; ?>

                                        <?php if(isset($recentPosts[2])): ?>
                                            <div class="blog-box">
                                                <?php  if($featuredImg2 = get_the_post_thumbnail_url($recentPosts[2]['ID'], 'full') ) : ?>
                                                    <div class="blog-image blog-image-3">
                                                       <img src="<?php echo $featuredImg2; ?>" class="img-responsive" />
                                                    </div>
                                                <?php endif; ?>
                                                <div class="blog-text">
                                                    <h3><a href="<?php echo get_permalink($recentPosts[2]['ID']); ?>"><?php echo $recentPosts[2]['post_title'];?></a></h3>
                                                    <div class="author"><a href="<?php echo get_permalink($recentPosts[2]['ID']); ?>"><?php echo get_the_author_meta('display_name', $recentPosts[2]['post_author']) ;?>  |  <?php echo date('M d, Y', strtotime($recentPosts[2]['post_date']) ) ?></a></div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                    
                                    <div class="col-sm-6">
                                        <?php if(isset($recentPosts[3])): ?>
                                            <div class="blog-box">
                                                <?php  if($featuredImg3 = get_the_post_thumbnail_url($recentPosts[3]['ID'], 'full') ) : ?>
                                                    <div class="blog-image blog-image-4">
                                                       <img src="<?php echo $featuredImg3; ?>" class="img-responsive" />
                                                    </div>
                                                <?php endif; ?>
                                                <div class="blog-text">
                                                    <h3><a href="<?php echo get_permalink($recentPosts[3]['ID']); ?>"><?php echo $recentPosts[3]['post_title'];?></a></h3>
                                                    <div class="author"><a href="<?php echo get_permalink($recentPosts[3]['ID']); ?>"><?php echo get_the_author_meta('display_name', $recentPosts[3]['post_author']) ;?>  |  <?php echo date('M d, Y', strtotime($recentPosts[3]['post_date']) ) ?></a></div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                        <?php if(isset($recentPosts[4])): ?>
                                            <div class="blog-box change">
                                                <div class="blog-text brown">
                                                     <h3><a href="<?php echo get_permalink($recentPosts[4]['ID']); ?>"><?php echo $recentPosts[4]['post_title'];?></a></h3>
                                                    <div class="author"><a href="<?php echo get_permalink($recentPosts[4]['ID']); ?>"><?php echo get_the_author_meta('display_name', $recentPosts[4]['post_author']) ;?>  |  <?php echo date('M d, Y', strtotime($recentPosts[4]['post_date']) ) ?></a></div>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                    
                    <h1>Instagram</h1>
                    <div class="instagram-wrap row">
                        <div class="col-sm-12">
                            <div class="instagram-cont clearfix">
                                <div class="instagram-box">
                                    <div class="instag-img">
                                         <img src="<?php echo get_template_directory_uri() . '/images/image9.jpg'?>" class="img-responsive" />
                                    </div>
                                    <a href="#" class="video-cam">
                                        <span class="icon ion-ios-videocam"></span>
                                    </a>
                                </div>
                                <div class="instagram-box">
                                    <div class="instag-img">
                                         <img src="<?php echo get_template_directory_uri() . '/images/image10.jpg'?>" class="img-responsive" />
                                    </div>
                                </div>
                                <div class="instagram-box">
                                    <div class="instag-img">
                                         <img src="<?php echo get_template_directory_uri() . '/images/image11.jpg'?>" class="img-responsive" />
                                    </div>
                                    <div class="instag-text">
                                        <div>
                                            <div class="status clearfix">
                                                <div class="stat-left pull-left">
                                                    <span class="icon ion-ios-heart-outline"></span>
                                                    126
                                                </div>
                                                <div class="stat-right pull-right">
                                                    <span class="icon ion-ios-chatbubble-outline"></span>
                                                    126
                                                </div>
                                            </div>
                                            <p>Lorem ipsum dolor sit ame, consectetur adipisicing #elitsed do @eiusmod #tempor #incididunt ut labore et dolore magna aliqua.</p>                    
                                        </div>
                                    </div>
                                </div>
                                <div class="instagram-box">
                                    <div class="instag-img">
                                         <img src="<?php echo get_template_directory_uri() . '/images/image12.jpg'?>" class="img-responsive" />
                                    </div>
                                    <a href="#" class="video-cam">
                                        <span class="icon ion-ios-videocam"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php include(__DIR__ . '/sections/footer.php') ?>
</div>
<?php wp_footer(); ?>
</body>
</html>