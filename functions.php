<?php

function baggi_setup() {
	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
	 */
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 825, 510, true );

	register_nav_menus(
	    array(
	      'primary' => __( 'Primary Menu' ),
	      'dropdown_nav1' => __('Dropdown Nav1'),
	      'dropdown_nav2' => __('Dropdown Nav2')
	    )
	  );
	add_theme_support( 'custom-logo', array(
		'height'      => 128,
		'width'       => 100,
		'flex-height' => true,
	) );
}

add_action( 'after_setup_theme', 'baggi_setup' );

add_shortcode('site-url', function(){
	return get_bloginfo('url');
});

function baggi_scripts() {

	//wp_register_script( 'bootstrap', 'cres.js', null, '3.3.5', true );
	//wp_enqueue_script('bootstrap');
}
add_action( 'wp_enqueue_scripts', 'baggi_scripts' );
/*
function twentyfifteen_widgets_init() {
	register_sidebar( array(
		'name'          => __( 'Home Footer', 'baggi' ),
		'id'            => 'home-footer',
		'description'   => __( 'Add widgets here to appear in your home page Footer.', 'baggi' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'twentyfifteen_widgets_init' );
*/

