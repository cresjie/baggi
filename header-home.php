<?php include(__DIR__ . '/sections/header-head-section.php'); ?>
<div id="menu-wrap" class="active">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-12 logo-box clearfix">
            	<div class="logo">
                  <?php if(function_exists('the_custom_logo')): ?>
                	   <?php  the_custom_logo(); ?>
                  <?php endif; ?>
                </div>
                <div class="navbar pull-right">
                <div class="navbar-header">
                    <button aria-expanded="false" data-target="#menu-navbar-collapse-1" data-toggle="collapse" class="navbar-toggle collapsed menu-mobile" type="button"> <span class="icon ion-navicon"></span> </button>
                  </div>
                </div>
            </div>
            <div class="col-sm-12">
               <div class="menu">
            	<nav class="navbar navbar-default"> 
                  <!-- Brand and toggle get grouped for better mobile display -->                                    
                  <!-- MENU -->
                  <div id="menu-navbar-collapse-1" class="collapse navbar-collapse">
                    <?php wp_nav_menu( array( 
                            'theme_location' => 'primary',
                            'container' => false,
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>'
                        ) ); ?>

                    <!--
                    <ul class="nav navbar-nav">
                      <li class="home"><a href="#"><span>Home</span></a></li> 
                       <li> <a href="#"><span>About Us</span></a></li>
                      <li><a href="#"><span>blog</span></a></li>
                      <li><a href="#"><span>typography</span></a></li>             
                      <li><a href="#"><span>Contact us</span></a></li>                           
                    </ul>
                    -->
                  </div>
                  <!-- /.navbar-collapse --> 
                </nav>
                </div>
            </div>
        </div>               
    </div>
</div>

<?php include(__DIR__ . '/sections/header-second-menu.php'); ?>
