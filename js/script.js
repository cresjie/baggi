$(document).ready(function(){
	$.scrollify({
		section : ".animate",
		sectionName : ".animate",
		interstitialSection : "",
		easing: "easeOutExpo",
		scrollSpeed: 1100,
		offset : 0,
		scrollbars: true,
		standardScrollElements: "",
		setHeights: true,
		overflowScroll: true,
		updateHash: true,
		touchScroll:true,
		after:function() {},
	});
	$(".scroll,.scroll-btn").click(function(e) {
		e.preventDefault();
		$.scrollify.next();
	});
	
	$(".menu-mobile2").on('click',function(){

		var a = $(this);
		if($(this).is('.collapsed')){
			a.find('span').html("CLOSE");
			$('#second-menu').addClass('in');
			$('#second-menu .menu').css('display','block');
		} else {
			a.find('span').html("MENU");
			$('#second-menu').removeClass('in');
			$('#second-menu .menu').css('display','none');
			//setTimeout(function(){  }, 300);
		}
	});
	
	$(window).scroll(function(event){ 
		var scrollTop = $(this).scrollTop();
		if(scrollTop >= 150){
			$('#menu-wrap').removeClass('active');
			$('#second-menu').addClass('active');			
		} else{
			$('#second-menu').removeClass('active');
			$('#menu-wrap').addClass('active');
			
		}
	});
});