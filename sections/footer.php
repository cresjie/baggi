<footer id="footer">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="footer-logo">
                        <a href="#">
                             <img src="<?php echo get_template_directory_uri() . '/images/footer-logo.png'?>" class="img-responsive" />
                        </a>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="footer-contact">
                        <address>
                        1558 EGLINTON AVE WEST 
                         TORONTO ON M6E 2G8
                        </address>  
                        <div class="phone">
                            <div>Phone:     +1.4167894913</div>
                            <div>E-mail: info@baggimen.ca</div>
                        </div>
                        
                        <div class="social">
                            <a href="#" class="icon ion-social-facebook"></a>
                            <a href="#" class="icon ion-social-twitter"></a>
                            <a href="#" class="icon ion-social-pinterest"></a>
                            <a href="#" class="icon ion-social-youtube"></a>
                        </div>
                    </div>
                </div>
                
                <div class="col-sm-2 foot-one">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="<?php echo site_url('about-us')?>">About us</a></li>
                            <li><a href="#">Clothing</a></li>
                            <li><a href="<?php echo site_url('ready-made')?>">Ready made</a></li> 
                            <li><a href="<?php echo site_url('made-to-measure')?>">Made to mesure</a></li>
                            <li><a href="#">Souliers</a></li>
                            <li><a href="#">Chemise</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-2 foot-one">
                    <div class="footer-menu">
                        <ul>
                            <li><a href="#">Accessoires</a></li>
                            <li><a href="<?php echo site_url('rental')?>">Rental</a></li>
                            <li><a href="#">VIP</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Visite virtuelle</a></li>
                        </ul>
                    </div>
                </div>
                
                <div class="col-sm-3 foot-two">
                    <div class="subscriber">
                        <h3 class="subscribe-text">subscriber</h3>
                        <?php echo do_shortcode('[mc4wp_form id="25"]'); ?>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    <div id="copyright">
        <div class="container">
           <div class="copyright-box">
            <div class="row">
                <div class="col-sm-6">
                    © 2017 Baggi (Canada) Limited. All rights reserved.
                </div>  
                <div class="col-sm-6">
                   <div class="privacy">
                    <a href="<?php echo site_url('privacy-policy')?>">Privacy Policy </a> |
                    <a href="<?php echo site_url('terms-and-condition')?>">Terms and Conditions</a>
                   </div>
                </div>
            </div>
            </div>
        </div>
    </div>