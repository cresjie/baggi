<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo get_template_directory_uri() . '/css/bootstrap.min.css'; ?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() . '/css/ionicons.min.css'; ?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() . '/css/style.css' ?>" rel="stylesheet">
<link href="<?php echo get_template_directory_uri() . '/style.css' ?>" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet"> 
<script type="application/javascript" src="<?php echo get_template_directory_uri() . '/js/jquery.js'?>"></script>
<script src="<?php echo get_template_directory_uri() . '/js/bootstrap.js' ?>"></script>
<script type="application/javascript" src="<?php echo get_template_directory_uri() . '/js/jquery.scrollify.min.js'?>"></script>
<script type="application/javascript" src="<?php echo get_template_directory_uri() . '/js/script.js'?>"></script>
<?php wp_head() ?>
</head>

<body <?php body_class(); ?>>
