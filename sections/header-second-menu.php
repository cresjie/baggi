<div id="second-menu" class="<?php echo !is_front_page() ? 'inner':''?>">
	<div class="container">
    	<div class="row">
        	<div class="col-sm-12 logo-box clearfix">
            	<div class="logo pull-left">
                	<?php if(function_exists('the_custom_logo')): ?>
                     <?php  the_custom_logo(); ?>
                  <?php endif; ?>
                </div>
                <div class="navbar pull-right">
                <div class="navbar-header">
                    <button aria-expanded="false" data-target="#menu-navbar-collapse-2" data-toggle="collapse" class="navbar-toggle collapsed menu-mobile2" type="button"> <span class="icon ion-navicon">MENU</span> </button>
                  </div>
                </div>
            </div>
            <div class="col-sm-12 clearfix">
               <div class="menu">
            	<nav class="navbar navbar-default"> 
                  <!-- Brand and toggle get grouped for better mobile display -->                                    
                  <!-- MENU -->
                  <div id="menu-navbar-collapse-2" class="clearfix collapse">
                    <?php wp_nav_menu( array( 
                            'theme_location' => 'dropdown_nav1',
                            'container' => false,
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>'
                        ) ); ?>
                      <?php wp_nav_menu( array( 
                            'theme_location' => 'dropdown_nav2',
                            'container' => false,
                            'link_before' => '<span>',
                            'link_after' => '</span>',
                            'items_wrap' => '<ul class="nav navbar-nav">%3$s</ul>'
                        ) ); ?>
                        <!--
                    <ul class="nav navbar-nav">
                      <li class="home"><a href="#">Home</a></li> 
                       <li> <a href="#">About Us</a></li>
                      <li><a href="#">clothing</a></li>                                           
                    </ul>
                    <ul class="nav navbar-nav">
                      <li class="home"><a href="#">accessories</a></li> 
                       <li> <a href="#">rental</a></li>
                      <li><a href="#">contact</a></li>                                           
                    </ul>
                        -->
                  </div>
                  <!-- /.navbar-collapse --> 
                </nav>
                </div>
            </div>
        </div>               
    </div>
</div>