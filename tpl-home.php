<?php
/**
 * Template Name: Homepage
 */ 
	get_header('home'); 
?>
<div id="header-block" class="animate header-block" data-section-name="header-block">
    <div class="header-wrap">   
         <!--INTRODUCTION -->
        <div class="intro-wrap">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <div class="intro-box">
                            <a href="<?php echo site_url('made-to-measure') ?>" class="x"><span>Made To Measure</span></a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="intro-box">
                            <a href="<?php echo site_url('ready-made') ?>"><span>Ready Made</span></a>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <div class="intro-box">
                            <a href="<?php echo site_url('rental') ?>"><span>Rental</span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="scroll-down">
            <a href="#main-selection" class="scroll"><img src="<?php echo get_template_directory_uri() . '/images/scroll-down.png'?>" class="img-responsive" /></a>
        </div>
    </div>
   <div class="video-background">
        <div class="video-black-overlay"></div>
        <iframe width="100%" height="100%" src="https://www.youtube.com/embed/_qATv5xzVYs?mute=1&modestbranding=1&autoplay=1&loop=1&showinfo=0&controls=0" frameborder="0" gesture="media" allowfullscreen></iframe>
  </div>
</div>

<div id="main-selection" class="animate main-selection padTop" data-section-name="main-selection">
    <div class="container">
        <div class="main-select-wrap">
            <div class="row">
                <div class="col-sm-4">
                    <div class="main-select-image">
                        <div class="image-wrap">
                            <img src="<?php echo get_template_directory_uri() . '/images/image1.jpg'?>" class="img-responsive first-img" />
                            <img src="<?php echo get_template_directory_uri() . '/images/image1-hover.jpg'?>" class="img-responsive img-hover" />
                        </div>
                        <div class="main-select-text">
                          <div>
                            <h3><a href="<?php echo get_bloginfo('url') . '/made-to-measure' ?>">Made to Measure</a></h3>
                          </div>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="main-select-image">
                        <div class="image-wrap">
                            <img src="<?php echo get_template_directory_uri() . '/images/image2.jpg'?>" class="img-responsive first-img" />
                            <img src="<?php echo get_template_directory_uri() . '/images/image2-hover.jpg'?>" class="img-responsive img-hover" />
                        </div>
                        <div class="main-select-text">
                          <div>
                            <h3>
                                <a href="<?php echo get_bloginfo('url') . '/ready-made' ?>">Ready Made</a></h3>
                          </div>
                        </div>
                    </div>
                </div>
                 <div class="col-sm-4">
                    <div class="main-select-image">
                        <div class="image-wrap">
                            <img src="<?php echo get_template_directory_uri() . '/images/image3.jpg'?>" class="img-responsive first-img" />
                            <img src="<?php echo get_template_directory_uri() . '/images/image3-hover.jpg'?>" class="img-responsive img-hover" />
                        </div>
                        <div class="main-select-text">
                          <div>
                            <h3>
                                <a href="<?php echo get_bloginfo('url') . '/rental' ?>">
                                    Rental
                                </a>
                            </h3>
                           </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    <div class="scroll-down">
        <a href="#last-selection" class="scroll-btn"><img src="<?php echo get_template_directory_uri() . '/images/keep-scroll.png'?>" class="img-responsive" /></a>
    </div>
</div>


<?php get_footer('instagram'); ?>
