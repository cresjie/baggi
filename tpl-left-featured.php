<?php
/**
 * Template Name: Left Featured Img
 */ 
	get_header(); 
?>

<div class="page-body body">
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
	        <div class="main-select-wrap">
	            <div class="row">
	                
	                
	                <div class="col-sm-5">
	                	<?php if ( has_post_thumbnail() ): ?>
	                		<div class="inner-image">
		                		<img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive first-img" />
		                	</div>
	                	<?php endif; ?>
	                </div>
	                <div class="col-md-7">
	                	
	                		<h1 class="page-title">
	                			<span><?php the_title(); ?></span>
	                		</h1>
	                	
	                	<div class="page-content">
	                		<?php the_content(); ?>
	                	</div>
	                </div>
	                	
	            </div>
	        </div>
	    </div>
	    
	    
	<?php endwhile ; ?>
	<?php get_footer(); ?>
</div>
