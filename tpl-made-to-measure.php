<?php
/**
 Template Name: Made to Measure
 */
	get_header(); 
	the_post();
?>

<div class="page-body body">
	<div class="animate measure-section1"
		<?php if ( has_post_thumbnail() ): ?>
			style="background-image: url(<?php the_post_thumbnail_url('full');?>)"
		<?php endif; ?>
	>
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	 <div class="measure-text">
                     	<h1><span><?php the_title(); ?></span></h1>
                     </div>
                	 <div class="scroll-box">
                    	<a href="#measure-section2" class="scroll">SCROLL DOWN</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animate measure-section2 team-page" id="measure-section2" data-section-name="measure-section2">
    	<div class="container">
        	<div class="row">
            	<div class="col-sm-12">
                	<div class="measure-section2-box">
                    	<div class="row">
                        	<div class="col-sm-4">
                            	<div class="ms-box">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image28.jpg" class="img-responsive" />
                                    </div>
                                	<div class="ms-text">
                                    	<div>
                                        	Custom Measurements
                                        </div>
                                    </div>
                                </div>
                                <div class="ms-box">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image29.jpg" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            	 <div class="ms-box a1">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image30.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                 <div class="ms-box a1">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image31.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                 <div class="ms-box a1">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image32.jpg" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                            	 <div class="ms-box change">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image33.jpg" class="img-responsive" />
                                    </div>
                                </div>
                                 <div class="ms-box">
                                	<div class="ms-image">
                                    	 <img src="<?php echo get_template_directory_uri()?>/images/image34.jpg" class="img-responsive" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="animate team-page measure-section3">
        <div class="measure-section3-image">
        </div>

         	<div class="container main">
            	<?php the_content(); ?>
            </div>
            
        <?php get_footer(); ?>
        
    </div>
	
</div>