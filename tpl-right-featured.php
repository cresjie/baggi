<?php
/**
 * Template Name: Right Featured Img
 */ 
	get_header(); 
?>

<div class="page-body body">
	<?php while ( have_posts() ) : the_post(); ?>
		<div class="container">
	        <div class="main-select-wrap">
	            <div class="row">
	                
	                <div class="col-md-7">
	                	
	                		<h1 class="page-title">
	                			<span><?php the_title(); ?></span>
	                		</h1>
	                	
	                	<div class="page-content">
	                		<?php the_content(); ?>
	                	</div>
	                </div>
	                <div class="col-sm-5">
	                	<?php if ( has_post_thumbnail() ): ?>
	                		<div class="inner-image">
		                		<img src="<?php the_post_thumbnail_url('full');?>" class="img-responsive first-img" />
		                	</div>
	                	<?php endif; ?>
	                </div>
	                	
	            </div>
	        </div>
	    </div>
	    
	    <div class="scroll-down">
	    	<a href="#last-selection" class="scroll-btn"><img src="<?php echo get_template_directory_uri() . '/images/keep-scroll.png'?>" class="img-responsive" /></a>
	    </div>
	<?php endwhile ; ?>
	<?php get_footer(); ?>
</div>



